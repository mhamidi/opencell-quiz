function getUsers() {
    return fetch("https://jsonplaceholder.typicode.com/users")
      .then(handleErrors)
      .then(res => res.json());
  }
  
  export function fetchUsers() {
    return dispatch => {
      return getUsers()
        .then(json => {
          dispatch(fetchUsersSuccess(json));
          return json;
        })
        .catch(error =>
          dispatch(fetchUsersFailure(error))
        );
    };
  }
  
  function handleErrors(response) {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  }
  
  export const FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS";
  export const FETCH_USERS_FAILURE = "FETCH_USERS_FAILURE";
  
  export const fetchUsersSuccess = users => ({
    type: FETCH_USERS_SUCCESS,
    payload: { users }
  });
  
  export const fetchUsersFailure = error => ({
    type: FETCH_USERS_FAILURE,
    payload: { error }
  });
