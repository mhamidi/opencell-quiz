function getPosts() {
    return fetch("https://jsonplaceholder.typicode.com/posts")
      .then(handleErrors)
      .then(res => res.json());
  }
  
  export function fetchPosts() {
    return dispatch => {
      return getPosts()
        .then(json => {
          dispatch(fetchPostsSuccess(json));
          return json;
        })
        .catch(error =>
          dispatch(fetchPostsFailure(error))
        );
    };
  }
  
  function handleErrors(response) {
    if (!response.ok) {
      throw Error(response.statusText);
    }
    return response;
  }
  
  export const FETCH_POSTS_SUCCESS = "FETCH_POSTS_SUCCESS";
  export const FETCH_POSTS_FAILURE = "FETCH_POSTS_FAILURE";
  
  export const fetchPostsSuccess = posts => ({
    type: FETCH_POSTS_SUCCESS,
    payload: { posts: posts.slice(0, 15) }
  });
  
  export const fetchPostsFailure = error => ({
    type: FETCH_POSTS_FAILURE,
    payload: { error }
  });
