import {
    FETCH_USERS_SUCCESS,
    FETCH_USERS_FAILURE
  } from "../actions/userActions";
  
  const initialState = {
    users: [],
    error: null
  };
  
  export default function userReducer(
    state = initialState,
    action
  ) {
    switch (action.type) {
      case FETCH_USERS_SUCCESS:
        return {
          ...state,
          users: action.payload.users
        };
  
      case FETCH_USERS_FAILURE:
        return {
          ...state,
          error: action.payload.error,
          users: []
        };
  
      default:
        return state;
    }
  }
  