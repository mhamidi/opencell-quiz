import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import React from "react";
import Comments from "../comments/Comments";

import { Link } from "react-router-dom";

class PostDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      post: {},
      user: {},
      company: {},
      comments: []
    };
  }

  fetchData = async (url) => {
    try {
        console.log(url);
      let response = await fetch(url);
      return await response.json();
    } catch (error) {
      console.error(error);
    }
  };

  componentDidMount() {
    const { postId } = this.props.match.params;
    let userId = null;
    this.fetchData(
      `https://jsonplaceholder.typicode.com/posts/${postId}`
    ).then(data => {
        this.setState({ post: data });
        userId = data.userId;
      }).then(() => this.fetchData(
        `https://jsonplaceholder.typicode.com/users/${userId}`
      ).then(data => {
        this.setState({ 
          user: data ,
          company: data.company
        });
      }));
    this.fetchData(
      `https://jsonplaceholder.typicode.com/comments?postId=${postId}`
    ).then(data => this.setState({ comments: data })); 
  }

  render() {
    return (
      <Card style={{backgroundColor: '#D6DBDF', width: '70%'}}>
        <CardContent >
          <Typography>Title: {this.state.post.title}</Typography>
          <Typography variant="body1" component="p">
            Content: {this.state.post.body}
          </Typography>
          <br/>

          <Link to={`/users/${this.state.user.id}`} >
            <Button color="primary" size="small">{this.state.user.name} [Company: {this.state.company.name}]</Button>
          </Link>
          
        </CardContent>
        
        <Comments comments={this.state.comments} />
      </Card>
    );
  }
}

export default PostDetail;
