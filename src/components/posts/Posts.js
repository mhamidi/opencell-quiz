import React from "react";
import { connect } from "react-redux";
import { fetchPosts } from "../../store/actions/postActions";
import Post from "./Post";


class Posts extends React.Component {
  
  componentDidMount() {
    this.props.dispatch(fetchPosts());
  }
  
  render() {
    const { error, posts } = this.props;

    if (error) {
      return <div>Error! {error.message}</div>;
    }
    return(
      <div>
        {
          posts && posts.map(post => <Post key={post.id} post={post} />)
        }
      </div>
    )
    
  }
}

const mapStateToProps = state => ({
  posts: state.postReducer.posts,
  error: state.postReducer.error
});

export default connect(mapStateToProps)(Posts);
