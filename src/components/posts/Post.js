import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import { Link } from "react-router-dom";

const useStyles = makeStyles({
  card: {
    width: '70%',
    marginBottom: '5px',
  },
  title: {
    fontSize: 14,
    color: "grey",
    textTransform: "uppercase",
    textAlign: 'left'
  }
});

const Post = props => {
  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography
          className={classes.title}
          gutterBottom
        >
          {props.post.title}
        </Typography>
        {/* <Typography variant="body1" component="p">
          {props.post.body}
        </Typography> */}
      </CardContent>
      <CardActions>
        <Link to={`/posts/${props.post.id}`} >
          <Button color="primary" size="small">+ Details</Button>
        </Link>
      </CardActions>
    </Card>
  );
};

export default Post;
