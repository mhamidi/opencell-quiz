import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import React from "react";

class UserDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      user: {}
    };
  }

  fetchData = async (url) => {
    try {
      let response = await fetch(url);
      return await response.json();
    } catch (error) {
      console.error(error);
    }
  };

  componentDidMount() {
    const { userId } = this.props.match.params;
    this.fetchData(
      `https://jsonplaceholder.typicode.com/users/${userId}`
    ).then(data => { 
      this.setState({ user: data })
    });
  }

  render() {
    return (
      <Card>
        <CardContent>
          <Typography>Name : {this.state.user.name}</Typography>
          <Typography>Username : {this.state.user.username}</Typography>
          <Typography>Email : {this.state.user.email}</Typography>
        </CardContent>
      </Card>
    );
  }
}

export default UserDetail;
