import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

import { Link } from "react-router-dom";

const useStyles = makeStyles({
  card: {
    width: 400,
    marginBottom: "5px"
  },
  title: {
    fontSize: 14,
    color: "#34495E",
    textTransform: "uppercase",
    textAlign: "left"
  }
});

const User = props => {
  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography className={classes.title}>
          {props.user.name} <br />[{props.user.username}]
        </Typography>
      </CardContent>
      <CardActions>
        <Link to={`/users/${props.user.id}`}>
          <Button color="primary" size="small">
            + Details
          </Button>
        </Link>
      </CardActions>
    </Card>
  );
};

export default User;
