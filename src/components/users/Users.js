import React from "react";
import { connect } from "react-redux";
import { fetchUsers } from "../../store/actions/userActions";
import User from "./User";


class Users extends React.Component {

  componentDidMount() {
    this.props.dispatch(fetchUsers());
  }
  render() {
    const { error, users } = this.props;

    if (error) {
      return <div>Error! {error.message}</div>;
    }

    return (
      <div>
        {
          users.map(user => <User key={user.id} user={user} />)
        }
      </div>
    );
  }
}

const mapStateToProps = state => ({
  users: state.userReducer.users,
  error: state.userReducer.error
});

export default connect(mapStateToProps)(Users);
