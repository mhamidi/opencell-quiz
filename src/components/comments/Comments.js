import React from 'react';
import Container from '@material-ui/core/Container';
import Comment from './Comment';

const Comments = (props) => {
    return (
        <Container style={{ marginTop: '5px'}}>
          {props.comments &&
            props.comments.map(comment => <Comment key={comment.id} comment={comment} />)}
        </Container>
      );
}

export default Comments;
