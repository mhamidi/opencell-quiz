import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";

const useStyles = makeStyles({
  card: {
    width: '70%',
    marginBottom: '5px',
  },
  title: {
    fontSize: 14,
    color: "grey",
    textTransform: "uppercase",
    textAlign: 'left'
  }
});

const Comment = props => {
  const classes = useStyles();
  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography
          className={classes.title}
          gutterBottom
        >
          {props.comment.name}
        </Typography>
        <Typography
          className={classes.title}
        >
          {props.comment.email}
        </Typography>
        <Typography variant="body1" component="p">
          {props.comment.body}
        </Typography>
      </CardContent>
    </Card>
  );
};

export default Comment;
