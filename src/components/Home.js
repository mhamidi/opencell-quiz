import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import { makeStyles } from '@material-ui/core/styles';
import React from "react";

const useStyles = makeStyles({
  card: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

const Home = () => {
    const classes = useStyles();
    return (
        <div>
            <div style={{fontWeight: 200, textAlign: 'center'}}>OPENCELL REACT QUIZ</div>
            <Card className={classes.card}>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image="opencell-logo.png"
                        title="Opencell"
                    />
                </CardActionArea>
            </Card>
        </div>
    )
}

export default Home;
