import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import React from "react";
import { NavLink, Route, Switch } from "react-router-dom";
import "./App.css";
import Home from "./components/Home";
import NotFound from "./components/NotFound";
import PostDetail from "./components/posts/PostDetail";
import Posts from "./components/posts/Posts";
import UserDetail from "./components/users/UserDetail";
import Users from "./components/users/Users";





const App = () => {

    return (
        <div className="App-header">
            <AppBar position="static" color="default" style={{marginBottom: '20px'}}>
              <Toolbar>
                <Button color="inherit">
                  <NavLink exact to="/" activeClassName="App-link">Home</NavLink>
                </Button>
                <Button color="inherit">
                  <NavLink to="/posts" activeClassName="App-link">Posts</NavLink>
                </Button>
                <Button color="inherit">
                  <NavLink to="/users" activeClassName="App-link">Users</NavLink>
                </Button>
              </Toolbar>
            </AppBar>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/posts" component={Posts} />
              <Route exact path="/posts/:postId" component={PostDetail} />
              <Route exact path="/users" component={Users} />
              <Route exact path="/users/:userId" component={UserDetail} />
              <Route component={NotFound} />
            </Switch>
        </div>
    );
  
}

export default App;
